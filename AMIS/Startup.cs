﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AMIS.Startup))]
namespace AMIS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
