﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class Inventory
    {
        public Guid InventoryId { get; set; }
        public virtual Warehouse Warehouse { get; set; }
        public virtual Product Product { get; set; }
        public decimal Quantity { get; set; }
    }
}
