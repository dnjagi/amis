﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMIS.Data.BusinessModels;

namespace AMIS.Data.EF.Models
{
    public class Warehouse:BaseModel
    {
        public Guid WarehouseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public WarehouseType WarehouseType { get; set; }
    }
}
