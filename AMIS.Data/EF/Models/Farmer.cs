﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class Farmer:BaseModel
    {
        public Guid FarmerId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public string MobileNumber { get; set; }

        public virtual Cooperative Cooperative { get; set; }

    }
}
