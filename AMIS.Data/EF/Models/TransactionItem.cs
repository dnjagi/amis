﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class TransactionItem
    {
        public Guid TransactionItemId { get; set; }
        public decimal TareWeight { get; set; }
        public decimal FinalWeight { get; set; }
        public virtual Product Product { get; set; }
        public virtual Container Container { get; set; }
    }
}
