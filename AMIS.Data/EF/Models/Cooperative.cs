﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class Cooperative:BaseModel
    {
        
        public Guid CooperativeId { get; set; }
        public string Name { get; set; }
        public virtual Contact Contact { get; set; }

        public virtual ICollection<Farmer> Farmers { get; set; } 
    }
}
