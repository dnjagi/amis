﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class Container
    {
        public Guid ContainerId { get; set; }
        public string Name { get; set; }
        public virtual ContainerType ContainerType { get; set; }
    }
}
