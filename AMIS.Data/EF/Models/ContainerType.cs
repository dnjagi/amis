﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class ContainerType:BaseModel
    {
        public Guid ContainerTypeId { get; set; }
        public string ContainerTypeName { get; set; }

        public string Description { get; set; }
    }
}
