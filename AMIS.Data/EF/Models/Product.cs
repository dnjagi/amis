﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class Product:BaseModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public ProductType ProductType { get; set; }

        public virtual ICollection<ProductGrade> Grades { get; set; } 
    }
}
