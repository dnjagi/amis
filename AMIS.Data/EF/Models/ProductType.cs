﻿using System;

namespace AMIS.Data.EF.Models
{
    public class ProductType:BaseModel
    {
        public Guid ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
        public string Description { get; set; }
    }
}