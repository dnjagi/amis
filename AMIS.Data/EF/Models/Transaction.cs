﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMIS.Data.BusinessModels;

namespace AMIS.Data.EF.Models
{
    public class Transaction:BaseModel
    {
        public Guid TransactionId { get; set; }
        public TransactionType TransactionType { get; set; }

        public DateTime TransactionTime { get; set; }
        public virtual TransactionItem TransactionItems { get; set; }
    }
}
