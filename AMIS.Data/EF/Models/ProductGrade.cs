﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIS.Data.EF.Models
{
    public class ProductGrade
    {
        public Guid ProductGradeId { get; set; }
        public string ProductGradeName { get; set; }

        public virtual Product Product { get; set; }
    }
}
